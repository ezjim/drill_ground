#include <stdio.h>

#include "../jimlib/utils.h"

/* gcc ../jimlib/utils.h ../jimlib/utils.c dh_test.c -o dh_test -l curl -l m */

int main(int argc, const char *argv[]) {
    DH_ELE dh_ele;
    dh_ele = DH_init();
    DH_side_A(&dh_ele);
    DH_side_B(&dh_ele);
    DH_get_secret(&dh_ele);
    printf("dh_ele.p: %ld\n", dh_ele.p);
    printf("dh_ele.g: %ld\n", dh_ele.g);
    printf("dh_ele.a: %ld\n", dh_ele.a);
    printf("dh_ele.b: %ld\n", dh_ele.b);
    printf("dh_ele.A: %ld\n", dh_ele.A);
    printf("dh_ele.B: %ld\n", dh_ele.B);
    printf("dh_ele.rA: %ld\n", dh_ele.rA);
    printf("dh_ele.rB: %ld\n", dh_ele.rB);
    
    return 0;
}
