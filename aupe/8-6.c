#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
int main(int argc, const char *argv[]) {
    pid_t pid;
    char cmd[BUFSIZ] = "ps aux | grep Z | grep -v grep";
    if (0 > (pid = fork())) {
        printf("error by fork!\n");
    } else if (0 == pid) {
        printf("Bye bye!\n");
        return 0;
    }
    // if (0 > waitpid( pid, NULL, 0)) {
    //     printf("waitpid error!\n");
    // }
    sleep(1);
    system(cmd);
    return 0;
}
