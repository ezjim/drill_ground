#ifndef _SIGNAL_HANDLER_H_
#define _SIGNAL_HANDLER_H_
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <signal.h>
#include <sys/errno.h>
#include <sys/resource.h>

extern void reread_config();
extern void sigterm(int signo);

extern void sighup(int signo);

extern void signal_handler();
#endif
