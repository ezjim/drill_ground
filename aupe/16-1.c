#include <stdio.h>

int main(int argc, const char *argv[]) {
    // 0x30 in ASCII '0';
    // 0x41 in ASCII 'A';
    unsigned short int num=0x4130;
    char *pNum;
    pNum = (char *) &num;
    if ('0' == pNum[0] && 'A' == pNum[1]) {
        printf("This system use little-endian!\n");
    } else {
        printf("This system use big-endian!\n");
    }
    printf("pNum[0]: %#X in ASCII %c\n", pNum[0], pNum[0]);
    printf("pNum[1]: %#X in ASCII %c\n", pNum[1], pNum[1]);
    return 0;
}
