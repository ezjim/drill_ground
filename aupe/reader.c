#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

int main(int argc, const char *argv[]) {
    char buf[BUFSIZ];
    int idx, fd1;
    printf("argc %d\n", argc);
    for (idx = 1; idx < argc; idx++) {
        fd1 = open((char *) argv[idx], O_RDONLY);
        bzero(buf, BUFSIZ);
        read(fd1, buf, BUFSIZ);
        close(fd1);
        printf("%s", buf);
    }
    sleep(86400);
    return 0;
}
