#include <stdio.h>
#include <unistd.h>     // For pipe;
#include <string.h>     // For bzero;
#include <sys/select.h>

#define MAXLINE 1024

int main(int argc, const char *argv[]) {
    int fd0[2], fd1[2], fd2[2], rst_n, fd_counter, w_counter = 1, w_cycle = 3, i, n;
    fd_set rset, wset;
    char line_buf[MAXLINE];
    bzero(line_buf, MAXLINE);
    pid_t pid;
    if (2 <= argc) {
        w_cycle = atoi(argv[1]);
    }
    // select中，单个读、写文件描述符的关闭不具代表性，故多添加一个；
    pipe(fd0);
    pipe(fd1);
    pipe(fd2);
    if (0 < (pid = fork())) {
        close(fd1[1]);
        close(fd2[0]);
        FD_ZERO(&rset);
        FD_ZERO(&wset);
        FD_SET(fd1[0], &rset);
        FD_SET(fd0[1], &wset);
        FD_SET(fd2[1], &wset);
        fd_counter = fd2[1] + 1;
        sleep(1);
        while(1) {
            rst_n = select(fd_counter, NULL, &wset, NULL, NULL);
            if (0 < rst_n) {
                for(i=0; i < fd_counter; i++) {
                    if (FD_ISSET(i, &wset) && i != fd0[1]) {
                        n = write(i, "something...", sizeof("something..."));
                        printf("write: something...\n");
                    }
                }
            } else if (0 == rst_n) {
                printf("Interrupted due to timeout!\n"); break; 
            } else {
                fprintf(stderr, "Interrupted due to a signal!\n"); break;
            }
            rst_n = select(fd_counter, &rset, NULL, NULL, NULL);
            if (0 < rst_n) {
                for(i=0; i < fd_counter; i++) {
                    if (FD_ISSET(i, &rset)) {
                        bzero(line_buf, MAXLINE);
                        n = read(i, line_buf, MAXLINE);
                        printf("read: %s\n", line_buf);
                    }
                }
            }
            w_counter++;
            if (w_cycle <= w_counter) {
                close(fd2[1]);
                printf("close fd2[1] w_counter: %d\n", w_counter);
            }
        }
        waitpid(pid, NULL, 0);
    } else {
        close(fd1[0]);
        close(fd2[1]);
        FD_ZERO(&rset);
        FD_SET(fd0[0], &rset);
        FD_SET(fd2[0], &rset);
        fd_counter = fd2[0] + 1;
        while(1) {
            rst_n = select(fd_counter, &rset, NULL, NULL, NULL);
            if (0 < rst_n) {
                for(i=0; i < fd_counter; i++) {
                    if (FD_ISSET(i, &rset)) {
                        n = read(i, line_buf, MAXLINE);
                        if (0 == n) {
                            fprintf(stderr, "Child: Pipeline peer side Close\n"); break;
                        }
                        n = write(fd1[1], line_buf, n);
                    }
                }
            } else if (0 == rst_n) {
                printf("Child: Interrupted due to timeout!\n");
            } else {
                fprintf(stderr, "Child: Interrupted due to a signal!\n"); break;
            }
            w_counter++;
            if (w_cycle+1 <= w_counter) {
                sleep(1);
                close(fd2[0]);
                printf("Child: Close fd2[0] w_counter: %d\n", w_counter);
            }
        }
    }
    return 0;
}
