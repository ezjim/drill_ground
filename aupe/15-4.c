#include <stdio.h>
#include <unistd.h>     // For pipe;
#include <string.h>     // For strlen;
#include <signal.h>

#define MAXLINE 1024
static void sig_pipe(int signo) {
    printf("SIGPIPE caught\n");
    _exit(1);
}

int main(int argc, const char *argv[]) {
    int n, fd1[2], fd2[2];
    pid_t pid;
    char line[MAXLINE];
    // if (SIG_ERR == signal(SIGPIPE, sig_pipe)) {
    //     printf("signal error\n"); _exit(-1);
    // }
    if (0 > pipe(fd1) || 0 > pipe(fd2)) {
        printf("pipe error\n"); _exit(-1);
    }
    if (0 > (pid = fork())) {
        printf("fork error\n"); _exit(-1);
    } else if (0 < pid) {
        close(fd1[0]);
        close(fd2[1]);
        while (NULL != fgets(line, MAXLINE, stdin)) {
            n = strlen(line);
            if (n != write(fd1[1], line, n)) {
                printf("write error to pipe\n"); _exit(-1);
            }
            if (0 > (n = read(fd2[0], line, MAXLINE))) {
                printf("read error from pipe\n"); _exit(-1);
            }
            if (0 == n) {
                printf("child closed pipe\n"); _exit(-1);
                break;
            }
            line[n] = 0;
            if (EOF == fputs(line, stdout)) {
                printf("fputs error\n"); _exit(-1);
            }
        }
        if (ferror(stdin)) {
            printf("fgets error on stdin\n"); _exit(-1);
        }
        return 0;
    } else {
        _exit(1);
        close(fd1[1]);
        close(fd2[0]);
        if (STDIN_FILENO != fd1[0]) {
            if (STDIN_FILENO != dup2(fd1[0], STDIN_FILENO)) {
                printf("dup2 error to stdin\n"); _exit(-1);
            }
            close(fd1[0]);
        }
        if (STDOUT_FILENO != fd2[1]) {
            if (STDOUT_FILENO != dup2(fd2[1], STDOUT_FILENO)) {
                printf("dup2 error to stdout\n"); _exit(-1);
            }
            close(fd2[1]);
        }
        if (0 > execl("/usr/bin/bc", "bc", (char *)0)) {
            printf("execl error\n"); _exit(-1);
        }
        return 0;
    }
    return 0;
}
