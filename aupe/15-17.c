#include <stdio.h>
#include <sys/sem.h>    // For semget...;
#include <sys/shm.h>    // For shmget...;
#include <fcntl.h>

#define SIZE sizeof(long)

union semun {
    int              val;    /* Value for SETVAL */
    struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
    unsigned short  *array;  /* Array for GETALL, SETALL */
    struct seminfo  *__buf;  /* Buffer for IPC_INFO
                                (Linux-specific) */
};

static int update(long *ptr) {
    return((*ptr)++);
}

int main(int argc, const char *argv[]) {
    int fd, shm_id, i, counter, nloops = 10;
    char path[BUFSIZ] = "./tmpfile", flag = 'p';
    pid_t pid;
    void *shm_area;
    fd = open( path, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
    unlink( path);
    pwrite( fd, &flag, sizeof(flag), 0);
    shm_id = shmget(IPC_PRIVATE, SIZE, IPC_CREAT);
    if (2 <= argc) {
        nloops = atoi(argv[1]);
    }
    if (0 > (pid = fork())) {
        fprintf(stderr, "fork error"); _Exit(-1);
    } else if (0 < pid) {
        shm_area = shmat(shm_id, 0, 0);
        for (i=0; i<nloops; i+=2) {
            // 下面while代码块即类 WAIT_CHILD();
            while( 'p' != flag) {
                if (-1 == pread( fd, &flag, sizeof(flag), 0)) {
                    printf("Error by child read!\n");
                }
            }
            if (i != (counter = update((long *)shm_area))) {
                fprintf(stderr, "parent: expected %d, got %d", i, counter); _Exit(-1);
            }
            printf("%c[7;32mparent current | i:%d | update() return: %d | share value: "\
                    "%2ld|%c[0m\n", 27, i, counter, *(long *)shm_area, 27);
            // 下面代码块即类 TELL_CHILD();
            flag = 'c';
            if (-1 == pwrite( fd, &flag, sizeof(flag), 0)) {
                printf("Error by child write!\n");
            }
        }
        shmdt(shm_area);
    } else {
        shm_area = shmat(shm_id, 0, 0);
        for (i=1; i<nloops+1; i+=2) {
            // 下面while代码块即类 WAIT_PARENT();
            while( 'c' != flag) {
                if (-1 == pread( fd, &flag, sizeof(flag), 0)) {
                    printf("Error by child read!\n");
                }
            }
            if (i != (counter = update((long *)shm_area))) {
                fprintf(stderr, "child: expected %d, got %d", i, counter); _Exit(-1);
            }
            printf("%c[7;33mchild  current | i:%d | update() return: %d | share value: "\
                    "%2ld|%c[0m\n", 27, i, counter, *(long *)shm_area, 27);
            // 下面代码块即类 TELL_PARENT();
            flag = 'p';
            if (-1 == pwrite( fd, &flag, sizeof(flag), 0)) {
                printf("Error by child write!\n");
            }
        }
        shmdt(shm_area);
    }
    waitpid(pid, NULL, 0);
    return 0;
}
