#include <stdio.h>
#include <sys/ipc.h>    // For ftok;
#include <sys/msg.h>    // For msgget;
#include <string.h>     // For memcpy;

#define MSGCNTLEN 128   // message content length;

struct msgsrt {
    long mtype;
    char mtext[MSGCNTLEN];
};

int main(int argc, const char *argv[]) {
    char *pjt_path = "/dev/zero"; // Project path;
    int queue_id, i = 1;
    pid_t pid;
    struct msgsrt msg;
    key_t queue_key = ftok(pjt_path, 1);
    char cnt[] = "content", cmdstr[4096];
    int cnt_len = sizeof(cnt), cycle = 10;
    if (2 <= argc) {
        cycle = atoi(argv[1]);
    }
    msg.mtype = 1;
    memcpy(msg.mtext, cnt, cnt_len);
    system("ipcrm -a");
    while (i <= cycle) {
        i++;
        if (i <= 5) {
            queue_id = msgget(queue_key, IPC_CREAT);
            msgctl(queue_id, IPC_RMID, NULL);
        } else {
            queue_id = msgget(IPC_PRIVATE, IPC_CREAT);
            msgsnd(queue_id, &msg, MSGCNTLEN, 0);
        }
        sprintf(cmdstr, "echo -n \"queue_id: %X\t\"; echo \"obase=2; ibase=16; %X\" | bc", queue_id, queue_id);
        system(cmdstr);
        // sleep(1);
    }
    return 0;
}
