#include <stdio.h>
#include <string.h>         // For strerror;
#include <netdb.h>          // For struct sockaddr_in;
#include <errno.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <signal.h>

#define TRUE 1
#define FALSE 0
#define MAXADDRLEN 256

void sig_wait_fun(int signo) {
    while(-1 != wait(NULL)) {};
}

int listen_socket(const char *listen_addr, const uint16_t listen_port, 
        const int type, int qlen) {
    int sfd, bFlag = TRUE;
    struct sockaddr_in socket_addr;
    socket_addr.sin_family = AF_INET;
    socket_addr.sin_addr.s_addr = htonl(inet_addr(listen_addr));
    socket_addr.sin_port = htons(listen_port);
    sfd = socket(AF_INET, type, 0);
    if (-1 == sfd) {
        fprintf(stderr, "error by socket: %s\n", strerror(errno));
        return -1;
    }
    setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &bFlag, sizeof(bFlag));
    if (0 > bind(sfd, (struct sockaddr *) &socket_addr, sizeof(socket_addr))) {
        fprintf(stderr, "error by bind: %s\n", strerror(errno));
        return -1;
    }
    if (SOCK_STREAM == type || SOCK_SEQPACKET == type) {
        if (0 > listen(sfd, qlen)) {
            fprintf(stderr, "error by listen: %s\n", strerror(errno));
            return -1;
        }
    }
    return sfd;
}

void server(const int sfd, const int type) {
    FILE *fp;
    char buf[BUFSIZ];
    pid_t pid;
    // char *cmd_str = "/bin/ps -e|wc -l";
    char *cmd_str = "/bin/ps -e";
    if (SOCK_STREAM == type) {
        int clfd;
        for(;;) {
            clfd = accept(sfd, NULL, NULL);
            // 所有新来连接让子进程去处理具体逻辑；
            if (0 == (pid = fork())) {
                if (0 > clfd) {
                    fprintf(stderr, "accept error: %s", strerror(errno));
                    _Exit(1);
                }
                if (NULL == (fp = popen(cmd_str, "r"))) {
                    sprintf(buf, "error: %s\n", strerror(errno));
                    send(clfd, buf, strlen(buf), 0);
                } else {
                    sprintf(buf, "ppid: %d\n", getppid());
                    send(clfd, buf, strlen(buf), 0);
                    while(NULL != fgets(buf, BUFSIZ, fp)) {
                        send(clfd, buf, strlen(buf), 0);
                    }
                    pclose(fp);
                }
                // shutdown(clfd, SHUT_RDWR);
                close(clfd);
                _Exit(0);
            }
            // 或在此启用该close，或启用子进程中的shutdown；
            close(clfd);
        }
    } else if (SOCK_DGRAM == type) {
        socklen_t addr_len = MAXADDRLEN;
        char addr_buf[MAXADDRLEN];
        for(;;) {
            if (0 > recvfrom(sfd, buf, BUFSIZ, 0, (struct sockaddr *)addr_buf, &addr_len)) {
                fprintf(stderr, "error by recvfrom : %s\n", strerror(errno)); _Exit(1);
            }
            fprintf(stderr, "recv %s\n", buf);
            // 所有新来连接让子进程去处理具体逻辑；
            if (0 == (pid = fork())) {
                if (NULL == (fp = popen(cmd_str, "r"))) {
                    sprintf(buf, "error: %s\n", strerror(errno));
                    sendto(sfd, buf, strlen(buf), 0, (struct sockaddr *) addr_buf, addr_len);
                } else {
                    while(NULL != fgets(buf, BUFSIZ, fp)) {
                        sendto(sfd, buf, strlen(buf), 0, (struct sockaddr *) addr_buf, addr_len);
                    }
                    pclose(fp);
                    close(sfd);
                }
                _Exit(0);
            }
        }
    } else {
        fprintf(stderr, "error by transport type: %d\n", type);
        _Exit(1);
    }
}

int main(int argc, const char *argv[]) {
    int sockfd, err, n;
    char *LISTEN_ADDR = "0.0.0.0";
    uint16_t LISTEN_PORT = 9999;
    // 避免子进程变僵尸；
    signal(SIGCHLD, sig_wait_fun);
    if (-1 == (sockfd = listen_socket(LISTEN_ADDR, LISTEN_PORT, SOCK_DGRAM, 10))) {
        fprintf(stderr, "error by listen_socket: \n"); _Exit(1);
    }
    server(sockfd, SOCK_DGRAM);
    return 0;
}
