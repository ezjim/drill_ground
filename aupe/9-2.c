#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>

int main(int argc, const char *argv[]) {
    pid_t pid;
    struct stat sb;
    fstat(stdout->_fileno, &sb);
    printf("parent stdout dev = %d/%d, inode: %ld!\n", major(sb.st_dev), minor(sb.st_dev), sb.st_ino);
    if (0 > (pid = fork())) {
        printf("error by fork!\n");
    } else if (0 == pid) {
        printf("child pid: %d, sid: %d!\n", getpid(), getsid(getpid()));
        system("ps ax -o uid,gid,euid,user,tpgid,sid,pgid,ppid,pid,tty,stat,comm|egrep 'USER|9-2'");
        pid = setsid();
        printf("after the new session: \n");
        printf("child pid: %d, sid: %d!\n", getpid(), getsid(0));
        system("ps ax -o uid,gid,euid,user,tpgid,sid,pgid,ppid,pid,tty,stat,comm|egrep 'USER|9-2'");
        fstat(stdout->_fileno, &sb);
        printf("child stdout dev = %d/%d, inode: %ld!\n", major(sb.st_dev), minor(sb.st_dev), sb.st_ino);
        return 0;
    }
    printf("parent pid: %d, sid: %d!\n", getpid(), getsid(getpid()));
    sleep(1);
    return 0;
}
