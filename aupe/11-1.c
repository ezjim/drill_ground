#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

struct foo {
    int a, b, c, d;
};

static struct foo foop = { 1, 2, 3, 4};

void printfoo(const char *s, const struct foo *fp) {
    printf("%s", s);
    printf(" structure at 0x%lx\n", (long unsigned) fp);
    printf(" foo.a = %d\n", fp->a);
    printf(" foo.b = %d\n", fp->b);
    printf(" foo.c = %d\n", fp->c);
    printf(" foo.d = %d\n", fp->d);
}

void * thr_fn1(void *arg) {
    printfoo("thread 1:\n", &foop);
    foop.a = 5;
    foop.b = 6;
    foop.c = 7;
    foop.d = 8;
    pthread_exit((void *) &foop);
}

void * thr_fn2(void *arg) {
    printf("thread 2: ID is %ld\n", pthread_self());
    pthread_exit((void *) 0);
}

int main(int argc, const char *argv[]) {
    int err;
    pthread_t tid1, tid2;
    struct foo *fp_receiver;
    err = pthread_create(&tid1, NULL, thr_fn1, NULL);
    if (0 != err) {
        printf("can't create thread 1: %s\n", strerror(err)); _exit(-1);
    }
    err = pthread_join(tid1, (void *) &fp_receiver);
    if (0 != err) {
        printf("can't join with thread 1: %s\n", strerror(err)); _exit(-1);
    }
    sleep(1);
    printf("parent starting second thread\n");
    err = pthread_create(&tid2, NULL, thr_fn2, NULL);
    if (0 != err) {
        printf("can't create thread 1: %s\n", strerror(err)); _exit(-1);
    }
    sleep(1);
    printfoo("parent:\n", fp_receiver);
    return 0;
}
