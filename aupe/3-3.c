#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>

int main(int argc, const char *argv[]) {
    int fd1, fd2, fd3;
    char buf[BUFSIZ];
    char pathname[] = "/tmp/open_test.pid";
    sprintf(buf, "%d\n", getpid());
    fd1 = open(pathname, O_RDWR | O_CREAT);
    write(fd1, buf, strlen(buf));
    printf("\nfd1 current offset: %ld\n", lseek( fd1, 0, SEEK_CUR));
    fd2 = dup(fd1);
    printf("fd2 current offset: %ld\n", lseek( fd2, 0, SEEK_CUR));
    fd3 = open(pathname, O_RDWR);
    printf("fd3 current offset: %ld\n", lseek( fd3, 0, SEEK_CUR));
    sleep(10);
    unlink(pathname);
    return 0;
}
