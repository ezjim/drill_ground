#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

struct job {
    struct job *j_next;
    struct job *j_prev;
    pthread_t j_id;
    int j_NO;
};

static pthread_cond_t qready;
struct queue {
    struct job *q_head;
    struct job *q_tail;
    pthread_mutex_t q_lock;
};

static struct queue q_job;
static pthread_t tid1, tid2, tid_dmr;

int queue_init(struct queue *qp) {
    int err;
    qp->q_head = NULL;
    qp->q_tail = NULL;
    pthread_mutex_init( &qp->q_lock, NULL);
    pthread_cond_init( &qready, NULL);
    return 0;
}

void job_append(struct queue *qp, struct job *jp) {
    pthread_mutex_lock(&qp->q_lock);
    if (NULL == qp->q_head && NULL == qp->q_tail) {
        qp->q_head = jp;
        qp->q_tail = jp;
    }
    jp->j_prev = qp->q_tail;
    jp->j_next = qp->q_head;
    qp->q_tail->j_next = jp;
    qp->q_tail = jp;
    qp->q_head->j_prev = qp->q_tail;
    pthread_mutex_unlock(&qp->q_lock);
}

struct job * job_remove(struct queue *qp, struct job *jp) {
    struct job *next_job = NULL;
    pthread_mutex_lock(&qp->q_lock);
    if (jp == qp->q_head && jp == qp->q_tail) {
        qp->q_head = NULL;
        qp->q_tail = NULL;
    } else if (jp == qp->q_head) {
        qp->q_head = jp->j_next;
    } else if (jp == qp->q_tail) {
        qp->q_tail = jp->j_prev;
    }
    jp->j_prev->j_next = jp->j_next;
    jp->j_next->j_prev = jp->j_prev;
    // 如果jp是队列里面的最后一个元素，则让next_job值为NULL返回；
    if (jp->j_next != jp) {
        next_job = jp->j_next;
    }
    free(jp);
    pthread_mutex_unlock(&qp->q_lock);
    return next_job;
}

void * thr_worker(void *arg) {
    int i;
    struct job *jp = NULL;
    while(1) {
        pthread_mutex_lock( &q_job.q_lock);
        while(NULL == jp) {
            printf("tid %ld cond wait\n", pthread_self());
            pthread_cond_wait( &qready, &q_job.q_lock);
            jp = q_job.q_head;
        }
        if (pthread_equal( pthread_self(), jp->j_id)) {
            if (pthread_equal( pthread_self(), tid1)) {
                printf("%c[7;32mthread ID is %ld : %c[0m", 27, pthread_self(), 27);
            } else {
                printf("%c[7;33mthread ID is %ld : %c[0m", 27, pthread_self(), 27);
            }
            printf("job id: %d\n", jp->j_NO);
            pthread_mutex_unlock( &q_job.q_lock);
            // jp对象将被删除，这里迭代它的下一个对象给它；
            // job_remove 返回jp->j_next；
            jp = job_remove( &q_job, jp);
        } else {
            pthread_mutex_unlock(&q_job.q_lock);
            jp = q_job.q_head;
        }
    }
    pthread_exit((void *) 0);
}

void * thr_drummer(void *arg) {
    while(1) {
        if (NULL != q_job.q_head) {
            pthread_cond_signal( &qready);
            printf("tid %ld cond signal\n", pthread_self());
            sleep(1);
        }
    }
    pthread_exit((void *) 0);
}

void batch_added(int n) {
    int i;
    for (i=1; i<n; i++) {
        struct job *jb = malloc((unsigned int) sizeof(struct job));
        if (0 == rand() % 2) {
            jb->j_id = tid1;
        } else {
            jb->j_id = tid2;
        }
        jb->j_NO = i;
        printf("tid: %ld NO.: %d\n", jb->j_id, jb->j_NO);
        job_append( &q_job, jb);
    }
}

int main(int argc, const char *argv[]) {
    int err;
    queue_init( &q_job);
    srand( (unsigned int)time(0) );

    pthread_create( &tid1, NULL, thr_worker, NULL);
    pthread_create( &tid2, NULL, thr_worker, NULL);
    pthread_create( &tid_dmr, NULL, thr_drummer, NULL);

    batch_added(100);
    sleep(5);
    batch_added(100);
    batch_added(100);
    batch_added(1000);
    sleep(5);
    batch_added(100);
    // 等待线程结束再退出进程；
    pthread_join( tid1, NULL);
    pthread_join( tid2, NULL);
    pthread_join( tid_dmr, NULL);

    return 0;
}
