#include <stdio.h>
#include <sys/ipc.h>    // For ftok;
#include <sys/msg.h>    // For msgget;
#include <string.h>     // For memcpy;
#include <errno.h>      // For errno;

#define MSGCNTLEN 128   // message content length;

struct msgsrt {
    long mtype;
    char mtext[MSGCNTLEN];
};

int main(int argc, const char *argv[]) {
    char *pjt_path = "/dev/zero"; // Project path;
    int queue_id, flag = 1, i = 0;
    pid_t pid;
    struct msgsrt msg;
    key_t queue_key = ftok(pjt_path, 1);
    queue_id = msgget(queue_key, IPC_CREAT);
    msgctl(queue_id, IPC_RMID, NULL);
    queue_id = msgget(queue_key, IPC_CREAT);
    printf("queue_id: %d\n", queue_id);
    printf("errno: %d %s\n", errno, strerror(errno));
    if (0 < (pid = fork())) {
        msg.mtype = 1;
        char cnt[] = "Counter ";
        int cnt_len = sizeof(cnt);
        while (i < 9) {
            i++;
            memcpy(msg.mtext, cnt, cnt_len);
            msg.mtext[cnt_len-1] = i+48;
            msg.mtext[cnt_len] = '\0';
            msgsnd(queue_id, &msg, MSGCNTLEN, 0);
        }
    } else {
        while (flag) {
            sleep(1);
            if (-1 == msgrcv(queue_id, &msg, MSGCNTLEN, 1, MSG_NOERROR | IPC_NOWAIT)) {
                break;
            }
            printf("read msg.mtext: %s\n", msg.mtext);
        }
    }
    return 0;
}
