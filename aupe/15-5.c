#include <stdio.h>
#include <unistd.h>     // For pipe;
#include <string.h>     // For strlen;
#include <signal.h>
#include <fcntl.h>

#define MAXLINE 1024
static void sig_pipe(int signo) {
    printf("SIGPIPE caught\n");
    _exit(1);
}

int main(int argc, const char *argv[]) {
    int n, fd1[2], fd2[2];
    FILE *fp1_1, *fp2_0;
    pid_t pid;
    char line[MAXLINE];
    if (SIG_ERR == signal(SIGPIPE, sig_pipe)) {
        printf("signal error\n"); _exit(-1);
    }
    if (0 > pipe(fd1) || 0 > pipe(fd2)) {
        printf("pipe error\n"); _exit(-1);
    }
    if (0 > (pid = fork())) {
        printf("fork error\n"); _exit(-1);
    } else if (0 < pid) {
        close(fd1[0]);
        close(fd2[1]);
        fp1_1 = fdopen(fd1[1], "w");
        fp2_0 = fdopen(fd2[0], "r");
        setvbuf(fp1_1, NULL, _IONBF, 0);
        // fcntl(fp2_0->_fileno, F_SETFL, O_NONBLOCK);     // 若用fread请开启此行；
        while (NULL != fgets(line, MAXLINE, stdin)) {
            n = strlen(line);
            if (n != fwrite(line, sizeof(char), n, fp1_1)) {
                printf("write error to pipe\n"); _exit(-1);
            }
            // 使用fread，关于读取多少返回的大小，无法提前获知；
            // 所以需要使用非阻塞模式，并且在读前等待些许时间，
            // 好让缓冲区在读前有时间被写入数据；
            // 若不然，fread将会一直阻塞，指到读取MAXLINE字节数据才返回；
            // sleep(1);    // 若用fread请开启此行；
            // if (0 > (n = fread(line, sizeof(char), MAXLINE, fp2_0))) {
            // 建议使用fgets，阻塞读取，直至返回1行；
            if (NULL == fgets(line, MAXLINE, fp2_0)) {
            // if (0 > (n = read(fd2[0], line, MAXLINE))) {
                printf("read error from pipe\n"); _exit(-1);
            }
            if (0 == n) {
                printf("child closed pipe\n"); _exit(-1);
                break;
            }
            line[n] = 0;
            if (EOF == fputs(line, stdout)) {
                printf("fputs error\n"); _exit(-1);
            }
        }
        if (ferror(stdin)) {
            printf("fgets error on stdin\n"); _exit(-1);
        }
        return 0;
    } else {
        close(fd1[1]);
        close(fd2[0]);
        if (STDIN_FILENO != fd1[0]) {
            if (STDIN_FILENO != dup2(fd1[0], STDIN_FILENO)) {
                printf("dup2 error to stdin\n"); _exit(-1);
            }
            close(fd1[0]);
        }
        if (STDOUT_FILENO != fd2[1]) {
            if (STDOUT_FILENO != dup2(fd2[1], STDOUT_FILENO)) {
                printf("dup2 error to stdout\n"); _exit(-1);
            }
            close(fd2[1]);
        }
        if (0 > execl("/usr/bin/bc", "bc", (char *)0)) {
            printf("execl error\n"); _exit(-1);
        }
        fprintf(stderr, "fff\n");
        return 0;
    }
    return 0;
}
