#include <stdio.h>
#include <sys/ipc.h>    // For ftok;
#include <sys/msg.h>    // For msgget;

#define MSGCNTLEN 128   // message content length;

struct msgsrt {
    long mtype;
    char mtext[MSGCNTLEN];
};

int main(int argc, const char *argv[]) {
    char *pjt_path = "/dev/zero"; // Project path;
    int queue_id, flag = 1, i = 0;
    struct msgsrt msg;
    key_t queue_key = ftok(pjt_path, 1);
    queue_id = msgget(queue_key, 0);
    sleep(3);
    while (i < 5) {
        i++;
        msgrcv(queue_id, &msg, MSGCNTLEN, 1, MSG_NOERROR);
        printf("other read msg.mtext: %s\n", msg.mtext);
    }
    return 0;
}
