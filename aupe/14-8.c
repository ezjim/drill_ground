#include <stdio.h>
#include <string.h>     // for strerror;
#include <unistd.h>     // for STDIN_FILENO;
#include <fcntl.h>      // for fcntl;
#include <errno.h>      // for errno;
char buf[100000];

int main(int argc, const char *argv[]) {
    int rn, wn;
    rn = read( STDIN_FILENO, buf, sizeof(buf));
    fprintf( stderr, "read %d bytes\n", rn);
    fcntl( STDOUT_FILENO, F_SETFL, O_NONBLOCK);
    fprintf( stderr, "_PC_PIPE_BUF %ld\n", fpathconf( STDOUT_FILENO, _PC_PIPE_BUF));
    while (0 < rn) {
        wn = write( STDOUT_FILENO, buf, rn);
        rn = rn - wn;
        fprintf( stderr, "%s\n", strerror(errno));
        fprintf( stderr, "write %d bytes\n", wn);
    }
    return 0;
}
