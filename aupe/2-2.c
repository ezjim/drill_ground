#include <stdio.h>
#include <errno.h>
#include <limits.h>

static void pr_sysconf(char *mesg, int name) {
    long val;
    fputs(mesg, stdout);
    errno = 0;
    if (0 > (val = sysconf(name))) {
        if (0 != errno) {
            if (EINVAL == errno) {
                fputs(" (not supported)\n", stdout);
            } else {
                fprintf(stderr, "sysconf error"); _Exit(-1);
            }
        } else {
            fputs(" (no limit)\n", stdout);
        }
    } else {
        printf(" %ld\n", val);
    }
}

static void pr_pathconf(char *mesg, char *path, int name) {
    long val;
    fputs(mesg, stdout);
    errno = 0;
    if (0 > (val = pathconf(path, name))) {
        if (0 != errno) {
            if (EINVAL == errno) {
                fputs(" (not supported)\n", stdout);
            } else {
                fprintf(stderr, "pathconf error, path = %s", path); _Exit(-1);
            }
        } else {
            fputs(" (no limit)\n", stdout);
        }
    } else {
        printf(" %ld\n", val);
    }
}

int main(int argc, const char *argv[]) {
    if (2 != argc) {
        fprintf(stderr, "usage: a.out <dirname>"); _Exit(-1);
    }
    #ifdef ARG_MAX
        printf("ARG_MAX deined to be %d\n", ARG_MAX+0);
    #else
        printf("no symbol for ARG_MAX\n");
    #endif
    #ifdef _SC_ARG_MAX
        pr_sysconf("ARG_MAX =", _AC_ARG_MAX);
    #else
        printf("no symbol for _SC_ARG_MAX\n");
    #endif
    #ifdef MAX_CANON
        printf("MAX_CANON defined to be %d\n", MAX_CANON+0);
    #else
        printf("no symbol for MAX_CANON\n");
    #endif
    #ifdef _PC_MAX_CANON
        pr_pathconf("MAX_CANON =", argv[1], _PC_MAX_CANON);
    #else
        printf("no symbol for _PC_MAX_CANON\n");
    #endif
    return 0;
}
