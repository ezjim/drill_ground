#include <stdio.h>
#include <fcntl.h>          // For open
#include <sys/types.h>      // pid_t;

static void charatatime(char *str) {
    char *ptr;
    int c;
    setbuf(stdout, NULL);
    for (ptr = str; 0 != (c = *ptr++); ) {
        usleep(10000);
        putc(c, stdout);
    }
}

int main(int argc, const char *argv[]) {
    printf("%d Begin!\n", getpid());
    pid_t pid;
    int fd;
    char path[BUFSIZ], flag = 'c';
    sprintf( path, "./pid_%d.flag", getpid());
    fd = open( path, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
    unlink( path);
    pwrite( fd, &flag, sizeof(flag), 0);
    if (0 > (pid = fork())) {
        printf("error by fork!\n");
    } else if (0 == pid) {
        pread( fd, &flag, sizeof(flag), 0);
        while('c' != flag) {
            pread( fd, &flag, sizeof(flag), 0);
        }
        charatatime("output from child\n");
        flag = 'p';
        pwrite( fd, &flag, sizeof(flag), 0);
        printf("%d Over!\n", getpid());
    } else {
        pread( fd, &flag, sizeof(flag), 0);
        while('p' != flag) {
            pread( fd, &flag, sizeof(flag), 0);
        }
        charatatime("output from parent\n");
        flag = 'c';
        pwrite( fd, &flag, sizeof(flag), 0);
    }
    return 0;
}
