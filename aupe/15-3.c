#include <stdio.h>
#include <unistd.h> // For pipe;

#define MAXLINE 1024

int main(int argc, const char *argv[]) {
    FILE *fpin;
    char line[MAXLINE];
    fpin = popen(argv[1], "r");
    system(argv[1]);
    while(NULL != fgets(line, MAXLINE, fpin)) {
        if (EOF == fputs(line, stdout)) {
            printf("fputs error to stdout\n"); _exit(-1);
        }
    }
    pclose(fpin);
    return 0;
}
