#include <stdio.h>
#include <time.h>

char str_date[BUFSIZ];
struct tm *pTm;

void now_time() {
    time_t ts = time(NULL);
    pTm = localtime(&ts);
    strftime( str_date, BUFSIZ, "%F %X", pTm);
    printf("%s\n", str_date);
}

int main(int argc, const char *argv[])
{
    setvbuf(stdout, NULL, _IONBF, 0);
    int counter = 0, cycle = 1, interval = 1;
    printf("Usage: ./10-10 interval(5) cycle(2)\n");
    if (3 <= argc) {
        interval = atoi(argv[1]);
        cycle = atoi(argv[2]);
    }
    while(1) {
        if (0 == counter % cycle) {
            now_time();
        }
        counter++;
        sleep(interval);
    }
    return 0;
}
