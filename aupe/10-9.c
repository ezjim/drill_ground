#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <signal.h>
#include <errno.h>

#define SIG2STR_MAX 256

int sig2str(int signo, char *str) {
    char tmp_str[SIG2STR_MAX], *pTmp_str = tmp_str;
    bzero(tmp_str, sizeof(SIG2STR_MAX));
    switch (signo) {
        // Reference '/usr/include/bits/signum.h';
        case SIGHUP:
            pTmp_str = "SIGHUP";
        break;
        case SIGINT:
            pTmp_str = "SIGINT";
        break;
        case SIGQUIT:
            pTmp_str = "SIGQUIT";
        break;
        case SIGILL:
            pTmp_str = "SIGILL";
        break;
        case SIGTRAP:
            pTmp_str = "SIGTRAP";
        break;
        case SIGABRT:
            pTmp_str = "SIGABRT|SIGIOT";
        break;
        case SIGBUS:
            pTmp_str = "SIGBUS";
        break;
        case SIGFPE:
            pTmp_str = "SIGFPE";
        break;
        case SIGKILL:
            pTmp_str = "SIGKILL";
        break;
        case SIGUSR1:
            pTmp_str = "SIGUSR1";
        break;
        case SIGSEGV:
            pTmp_str = "SIGSEGV";
        break;
        case SIGUSR2:
            pTmp_str = "SIGUSR2";
        break;
        case SIGPIPE:
            pTmp_str = "SIGPIPE";
        break;
        case SIGALRM:
            pTmp_str = "SIGALRM";
        break;
        case SIGTERM:
            pTmp_str = "SIGTERM";
        break;
        case SIGSTKFLT:
            pTmp_str = "SIGSTKFLT";
        break;
        case SIGCHLD:
            pTmp_str = "SIGCHLD|SIGCLD";
        break;
        case SIGCONT:
            pTmp_str = "SIGCONT";
        break;
        case SIGSTOP:
            pTmp_str = "SIGSTOP";
        break;
        case SIGTSTP:
            pTmp_str = "SIGTSTP";
        break;
        case SIGTTIN:
            pTmp_str = "SIGTTIN";
        break;
        case SIGTTOU:
            pTmp_str = "SIGTTOU";
        break;
        case SIGURG:
            pTmp_str = "SIGURG";
        break;
        case SIGXCPU:
            pTmp_str = "SIGXCPU";
        break;
        case SIGXFSZ:
            pTmp_str = "SIGXFSZ";
        break;
        case SIGVTALRM:
            pTmp_str = "SIGVTALRM";
        break;
        case SIGPROF:
            pTmp_str = "SIGPROF";
        break;
        case SIGWINCH:
            pTmp_str = "SIGWINCH";
        break;
        case SIGIO:
            pTmp_str = "SIGIO|SIGPOLL";
        break;
        case SIGPWR:
            pTmp_str = "SIGPWR";
        break;
        case SIGSYS:
            pTmp_str = "SIGSYS|SIGUNUSED";
        break;
        default:
            sprintf( tmp_str, "Does not recognize signo: %d", signo);
    }
    if (SIG2STR_MAX < strlen(tmp_str)) {
        sprintf( tmp_str, "You need more space in your str object!");
        return 1;
    }
    memcpy( str, pTmp_str, strlen(pTmp_str) + 1); // last is '\0';
    return 0;
}

int main(int argc, const char *argv[]) {
    sigset_t newsigset, oldsigset, cursigset;
    char sig_str[SIG2STR_MAX];
    int i, errno_save;
    sigemptyset(&newsigset);
    sigaddset(&newsigset, SIGUSR1);
    sigaddset(&newsigset, SIGUSR2);
    sigaddset(&newsigset, SIGKILL);
    sigaddset(&newsigset, SIGHUP);
    sigprocmask(SIG_BLOCK, &newsigset, &oldsigset);
    sigprocmask(SIG_BLOCK, NULL, &cursigset);
    for (i=1; i<=31; i++) {
        bzero(sig_str, sizeof(SIG2STR_MAX));
        errno_save = errno;
        if (sigismember(&cursigset, i)) {
            sig2str( i, sig_str);
        }
        errno = errno_save;
        if (0 < strlen(sig_str)) {
            printf("%s\n", sig_str);
        }
    }
    return 0;
}
