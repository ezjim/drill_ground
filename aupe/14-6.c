#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>

void sleep_us(int64_t us) {
    long us2s = 1000000;
    struct timeval tv;
    tv.tv_sec = us / us2s;
    tv.tv_usec = us % us2s;
    select( 0, NULL, NULL, NULL, &tv);
}

int main(int argc, const char *argv[]) {
    system("date");
    sleep_us(atoi(argv[1]));
    system("date");
    return 0;
}
