#include <stdio.h>
#include <sys/types.h>
#include <sys/signal.h>

void sig_fun(int signo) {
    printf( "ppid: %d, pid: %d, signo: %d\n", getppid(), getpid(), signo);
}

int main(int argc, const char *argv[]) {
    pid_t pid;
    signal(SIGHUP, sig_fun);        // 1
    signal(SIGINT, sig_fun);        // 2
    signal(SIGQUIT, sig_fun);       // 3
    signal(SIGABRT, sig_fun);       // 6
    signal(SIGKILL, sig_fun);       // 9
    signal(SIGTERM, sig_fun);       // 15
    while(1) {
        sleep(100);
        printf("Oh\n");
    }
    return 0;
}
