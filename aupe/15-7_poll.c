#include <stdio.h>
#include <unistd.h>     // For pipe;
#include <string.h>     // For bzero;
#include <poll.h>

#define MAXLINE 1024

int main(int argc, const char *argv[]) {
    int fd0[2], fd1[2], fd2[2], flag = 1, rst_n, fd_counter, w_counter = 1, w_cycle = 2, i, n;
    char line_buf[MAXLINE];
    bzero(line_buf, MAXLINE);
    struct pollfd pfdarr[1024];
    pid_t pid;
    if (2 <= argc) {
        w_cycle = atoi(argv[1]);
    }
    // poll中，单个读、写文件描述符的关闭不具代表性，故多添加一个；
    pipe(fd0);
    pipe(fd1);
    pipe(fd2);
    if (0 < (pid = fork())) {
        close(fd1[1]);
        close(fd2[0]);
        pfdarr[0].fd = fd1[0];
        pfdarr[0].events = POLLIN;
        pfdarr[1].fd = fd0[1];
        pfdarr[1].events = POLLOUT;
        pfdarr[2].fd = fd2[1];
        pfdarr[2].events = POLLOUT;
        fd_counter = 2 + 1;
        sleep(1);
        while(flag) {
            rst_n = poll(pfdarr, fd_counter, -1);
            printf("Parent: rst_n: %d\n", rst_n);
            if (0 < rst_n) {
                for(i=0; i < fd_counter; i++) {
                    printf("Parent: i: %d, fd: %d, revents: %d\n", i, pfdarr[i].fd, pfdarr[i].revents);
                    switch(pfdarr[i].revents) {
                        case POLLIN:
                        case POLLRDNORM:
                        case POLLRDBAND:
                        case POLLPRI: {
                                bzero(line_buf, MAXLINE);
                                n = read(pfdarr[i].fd, line_buf, MAXLINE);
                                printf("Parent: read: %s\n", line_buf);
                            }
                            break;
                        case POLLOUT:
                        case POLLWRNORM:
                        case POLLWRBAND: {
                                if (fd2[1] == pfdarr[i].fd) {
                                    n = write(pfdarr[i].fd, "something...", sizeof("something..."));
                                    printf("Parent: write: something...\n");
                                    // 等待切换到子进程读；
                                    sleep(1);
                                }
                            }
                            break;
                        case POLLERR: {
                                printf("Parent: error by POLLERR\n");
                                flag = 0;
                            }
                            break;
                        case POLLHUP: {
                                printf("Parent: POLLHUP\n");
                            }
                            break;
                        case POLLNVAL: {
                                printf("Parent: error by POLLNVAL\n");
                                flag = 0;
                            }
                            break;
                        default:
                            continue;
                    }
                }
            } else if (0 == rst_n) {
                printf("Parent: Interrupted due to timeout!\n"); break; 
            } else {
                fprintf(stderr, "Parent: Interrupted due to a signal!\n"); break;
            }
            w_counter++;
            if (w_cycle <= w_counter && flag) {
                close(fd2[1]);
                printf("Parent: close fd2[1] w_counter: %d\n", w_counter);
            }
        }
        waitpid(pid, NULL, 0);
    } else {
        close(fd1[0]);
        close(fd2[1]);
        pfdarr[0].fd = fd0[0];
        pfdarr[0].events = POLLIN;
        pfdarr[1].fd = fd2[0];
        pfdarr[1].events = POLLIN;
        fd_counter = 1 + 1;
        while(flag) {
            rst_n = poll(pfdarr, fd_counter, -1);
            if (0 < rst_n) {
                for(i=0; i < fd_counter; i++) {
                    printf("Child i: %d, fd: %d, revents: %d\n", i, pfdarr[i].fd, pfdarr[i].revents);
                    switch(pfdarr[i].revents) {
                        case POLLIN:
                        case POLLRDNORM:
                        case POLLRDBAND:
                        case POLLPRI: {
                                bzero(line_buf, MAXLINE);
                                n = read(pfdarr[i].fd, line_buf, MAXLINE);
                                if (0 == n) {
                                    fprintf(stderr, "Child: Pipeline peer side Close\n"); break;
                                }
                                n = write(fd1[1], line_buf, n);
                                // 等待切换到父进程读；
                                sleep(1);
                            }
                            break;
                        case POLLOUT:
                        case POLLWRNORM:
                        case POLLWRBAND: {
                                printf("Child: can wrint\n");
                            }
                            break;
                        case POLLERR: {
                                printf("Child: error by POLLERR\n");
                                flag = 0;
                            }
                            break;
                        case POLLHUP: {
                                printf("Child: POLLHUP\n");
                            }
                            break;
                        case POLLNVAL: {
                                printf("Child: error by POLLNVAL\n");
                                flag = 0;
                            }
                            break;
                        default:
                            continue;
                    }
                }
            } else if (0 == rst_n) {
                printf("Child: Interrupted due to timeout!\n");
            } else {
                fprintf(stderr, "Child: Interrupted due to a signal!\n"); break;
            }
            w_counter++;
            if (w_cycle+1 <= w_counter) {
                sleep(1);
                close(fd2[0]);
                printf("Child: Close fd2[0] w_counter: %d\n", w_counter);
            }
        }
    }
    return 0;
}
