#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

int main(int argc, const char *argv[]) {
    pid_t pid;
    if (0 > (pid = fork())) {
        printf("error by fork!\n");
    } else if (0 == pid) {
        if (0 > execlp("testinterp", "xxtestinterp", "myarg1", "MY ARG2", (char *) 0)) {
            printf("error by execlp!\n");
        }
    }
    if (0 > waitpid( pid, NULL, 0)) {
        printf("waitpid error!\n");
    }
    return 0;
}
