#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <fcntl.h>

int main(int argc, const char *argv[]) {
    DIR *pDir = NULL;
    pDir = opendir("/");
    printf( "FD_CLOEXEC by opendir: %d\n", fcntl( dirfd(pDir), F_GETFD));
    // (gdb) p (int) (DIR *) *pDir 
    printf( "FD_CLOEXEC by open: %d\n", fcntl( open("/", O_RDONLY), F_GETFD));
    return 0;
}
