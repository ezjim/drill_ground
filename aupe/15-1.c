#include <stdio.h>
#include <stdlib.h>     // For exit;
#include <unistd.h>     // For pipe;
#include <string.h>     // For strlen strrchr;
#define MAXLINE 1024
#define DEF_PAGER "/bin/more"

int main(int argc, const char *argv[]) {
    int n, fd[2];
    pid_t pid;
    char *pager, *argv0, line[MAXLINE];
    FILE *fp;
    if (2 != argc) {
        printf("usage: ./15-1 <pathname>\n"); _exit(-1);
    }
    if (NULL == (fp = fopen(argv[1], "r"))) {
        printf("can't open %s\n", argv[1]); _exit(-1);
    }
    if (0 > pipe(fd)) {
        printf("pipe error\n"); _exit(-1);
    }
    if (0 > (pid = fork())) {
        printf("fork error\n"); _exit(-1);
    } else if (0 < pid) {
        close(fd[0]);
        while(NULL != fgets(line, MAXLINE, fp)) {
            n = strlen(line);
            if (n != write(fd[1], line, n)) {
                printf("write error to pipe\n"); _exit(-1);
            }
        }
        if (ferror(fp)) {
            printf("fgets error\n"); _exit(-1);
        }
        close(fd[1]);
//         if (0 > waitpid(pid, NULL, 0)) {
//             printf("waitpid error\n"); _exit(-1);
//         }
        exit(0);
    } else {
        close(fd[1]);
        if (STDIN_FILENO != fd[0]) {
            if (STDIN_FILENO != dup2(fd[0], STDIN_FILENO)) {
                printf("dup2 error to stdin\n"); _exit(-1);
            }
            close(fd[0]);
        }
        if (NULL == (pager = getenv("PAGER"))) {
            pager = DEF_PAGER;
        }
        if (NULL != (argv0 = strrchr(pager, '/'))) {
            argv0++;
        } else {
            argv0 = pager;
        }
        printf("Child ppid: %d\n", getppid());
        if (0 > execl(pager, argv0, (char *)0)) {
            printf("execl error for %s\n", pager); _exit(-1);
        }
    }
    return 0;
}
