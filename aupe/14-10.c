#include <stdio.h>
#include <string.h>     // for memcpy;
#include <unistd.h>     // for _exit;
#include <fcntl.h>      // fro O_RDONLY...;
#include <sys/mman.h>   // for mmap...;

int main(int argc, const char *argv[]) {
    int fdin, fdout;
    void *src, *dst;
    struct stat statbuf;
    if (3 != argc) {
        printf("usage: %s <fromfile> <tofile>", argv[0]); _exit(-1);
    }
    if (0 > (fdin = open(argv[1], O_RDONLY))) {
        printf("can't open %s for reading", argv[1]); _exit(-1);
    }
    if (0 > (fdout = open(argv[2], O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR))) {
        printf("can't create %s for writing", argv[2]); _exit(-1);
    }
    if (0 > fstat(fdin, &statbuf)) {
        printf("fstat error"); _exit(-1);
    }
    if (-1 == lseek(fdout, statbuf.st_size - 1, SEEK_SET)) {
        printf("lseek error"); _exit(-1);
    }
    if (1 != write(fdout, "", 1)) {
        printf("write error"); _exit(-1);
    }
    if (MAP_FAILED == (src = mmap(0, statbuf.st_size, PROT_READ, MAP_SHARED, fdin, 0))) {
        printf("mmap error for input"); _exit(-1);
    }
    if (MAP_FAILED == (dst = mmap(0, statbuf.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, fdout, 0))) {
        printf("mmap error for output"); _exit(-1);
    }
    memcpy(dst, src, statbuf.st_size);
    return 0;
}
