#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <signal.h>

#define SIG2STR_MAX 256

int sig2str(int signo, char *str) {
    char tmp_str[SIG2STR_MAX], *pTmp_str = tmp_str;
    bzero(tmp_str, sizeof(SIG2STR_MAX));
    switch (signo) {
        case SIGUSR1: {
            pTmp_str = "SIGUSR1";
        }
        break;
        case SIGUSR2: {
            pTmp_str = "SIGUSR2";
        }
        break;
        default:
            sprintf( tmp_str, "Does not recognize signo: %d", signo);
    }
    if (SIG2STR_MAX < strlen(tmp_str)) {
        sprintf( tmp_str, "You need more space in your str object!");
        return 1;
    }
    memcpy( str, pTmp_str, strlen(pTmp_str) + 1); // last is '\0';
    return 0;
}

int main(int argc, const char *argv[]) {
    char sig_str[SIG2STR_MAX];
    if (2 > argc) {
        printf("Usage: ./10-2 12\n");
        return 1;
    }
    sig2str( atoi(argv[1]), sig_str);
    printf("%s\n", sig_str);
    return 0;
}
