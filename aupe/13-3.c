#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/resource.h>

#include "signal_handler.h"

void daemonize(const char *cmd) {
    int i, fd0, fd1, fd2;
    pid_t pid;
    struct rlimit rl;
    struct sigaction sa;
    umask(0);
    if (0 > getrlimit( RLIMIT_NOFILE, &rl)) {
        printf("%s: can't get file limit\n", cmd); _exit(-1);
    }
    if (0 > (pid = fork())) {
        printf("%s: can't fork", cmd); _exit(-1);
    } else if (0 != pid) {
        exit(0);
    }
    setsid();
    sa.sa_handler = SIG_IGN;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    if (0 > sigaction( SIGHUP, &sa, NULL)) {
        printf("%s: can't ignore SIGHUP", cmd); _exit(-1);
    }
    if (0 > (pid = fork())) {
        printf("%s: can't fork", cmd); _exit(-1);
    } else if (0 != pid) {
        exit(0);
    }
    if (0 > chdir("/")) {
        printf("%s: can't change directory to /", cmd); _exit(-1);
    }
    if (rl.rlim_max == RLIM_INFINITY) {
        rl.rlim_max = 1024;
    }
    for (i = 0; i < rl.rlim_max; i++) {
        close(i);
    }
    fd0 = open("/dev/null", O_RDWR);
    fd1 = dup(0);
    fd2 = dup(0);
}

int main(int argc, const char *argv[]) {
    daemonize(strrchr( argv[0], '/'));
    syslog( LOG_INFO, "login name %s", getlogin());
    signal_handler();
    while(1) {
        sleep(1);
    }
    return 0;
}
