// Usage: gcc -c signal_handler.c
//        gcc signal_handler.o main.c -o main
#include "signal_handler.h" 

extern void reread_config() {
    syslog( LOG_INFO, "config file rereaded");
}

extern void sigterm(int signo) {
    exit(0);
}

extern void sighup(int signo) {
    reread_config();
}

extern void signal_handler() {
    struct sigaction sa;
    sa.sa_handler = sigterm;
    // sigemptyset( &sa.sa_mask);
    sigfillset( &sa.sa_mask);
    // sigaddset( &sa.sa_mask, SIGHUP);
    sigdelset( &sa.sa_mask, SIGTERM);
    sa.sa_flags = 0;
    if (0 > sigaction( SIGTERM, &sa, NULL)) {
        syslog( LOG_ERR, "can't catch SIGTERM: %s", strerror(errno)); exit(0);
    }
    sa.sa_handler = sighup;
    // sigemptyset( &sa.sa_mask);
    sigfillset( &sa.sa_mask);
    // sigaddset( &sa.sa_mask, SIGTERM);
    sigdelset( &sa.sa_mask, SIGHUP);
    sa.sa_flags = 0;
    if (0 > sigaction( SIGHUP, &sa, NULL)) {
        syslog( LOG_ERR, "can't catch SIGHUP: %s", strerror(errno)); exit(1);
    }
}
