#include <stdio.h>
#include <unistd.h>         // For STDOUT_FILENO;
#include <string.h>         // For strerror;
#include <errno.h>
#include <netdb.h>          // For struct sockaddr_in;
#include <sys/socket.h>

int main(int argc, const char *argv[]) {
    int recv_c = 0, sockfd, listen_port;
    struct sockaddr_in dst_addr;
    char *listen_addr, buf[BUFSIZ];
    struct timeval tv;
    tv.tv_sec = 1;
    if (3 == argc) {
        listen_addr = (char *) argv[1];
        listen_port = atoi(argv[2]);
        printf("addr: %s, port: %d\n", listen_addr, listen_port);
    } else {
        fprintf(stderr, "Usage: %s server port\n", argv[0]);
        return -1;
    }
    dst_addr.sin_family = AF_INET;
    inet_pton(AF_INET, listen_addr, &dst_addr.sin_addr.s_addr);
    dst_addr.sin_port = htons(listen_port);
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    // 设置socket接收数据超时时间；
    setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));
    if (-1 == sockfd) {
        fprintf(stderr, "error by socket: %s\n", strerror(errno));
        return -1; 
    }
    if (0 > sendto(sockfd, "", sizeof(""), 0, (struct sockaddr *) &dst_addr, sizeof(dst_addr))) {
        fprintf(stderr, "error by sendto %s\n", strerror(errno));
    }
    while(0 < (recv_c = recvfrom(sockfd, buf, BUFSIZ, 0, NULL, NULL))) {
        write(STDOUT_FILENO, buf, recv_c);
    }
    return 0;
}
