#include <stdio.h>
#include <stdlib.h>     // for malloc;
#include <string.h>     // for strerror;
#include <unistd.h>     // for STDIN_FILENO;
#include <fcntl.h>      // for fcntl;
#include <errno.h>      // for errno;
#include <sys/uio.h>    // for writev;

int main(int argc, const char *argv[]) {
    int i, IOV_MAX = sysconf( _SC_IOV_MAX), IOV_CNT;
    char *pAction;
    struct iovec pIov[IOV_MAX];
    fprintf( stderr, "_SC_IOV_MAX %d\n", IOV_MAX);
    if (3 > argc) {
        fprintf( stderr, "You need assign amount and action\n"); _exit(-1);
    } else {
        IOV_CNT = atoi(argv[1]);
        pAction = (char *) argv[2];
    }
    if (IOV_MAX < atoi(argv[1])) {
        fprintf( stderr, "argv[1] should less than IOV_MAX: %d\n", IOV_MAX); _exit(-1);
    }
    for (i=0; i<IOV_CNT; i++) {
        pIov[i].iov_len = BUFSIZ * 100;
        pIov[i].iov_base = malloc(pIov[i].iov_len);
        read( STDIN_FILENO, pIov[i].iov_base, pIov[i].iov_len);
    }
    if (0 == strcmp("WR", pAction)) {
        fprintf( stderr, "in write\n");
        for (i=0; i<IOV_CNT; i++) {
            write( STDOUT_FILENO, pIov[i].iov_base, pIov[i].iov_len);
        }
    } else if (0 == strcmp("WV", pAction)) {
        fprintf( stderr, "in writev\n");
        writev( STDOUT_FILENO, pIov, IOV_CNT);
    } else {
        fprintf( stderr, "Please assign argv[2] in (WR|WV)\n");
    }
    return 0;
}
