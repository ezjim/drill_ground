#include <stdio.h>
#include <syslog.h>

int main(int argc, const char *argv[]) {
    if (2 <= argc) {
        syslog( LOG_ERR, "Syslog befor chroot %m");
    }
    chroot("/opt");
    syslog( LOG_ERR, "Syslog after chroot %m");
    return 0;
}
