#include <stdio.h>
#include <string.h>
#include <fcntl.h>      // For O_NONBLOCK...;
#include <sys/stat.h>   // For mkfifo;

#define MAXLINE 1024

int main(int argc, const char *argv[]) {
    int fffd, flag = 1, cycle = 3, cyc_counter = 0, n; // fffd for fifofd;
    pid_t pid;
    char *ffpath = "./a.fifo";
    char line_buf[MAXLINE];
    if (2 > argc) {
        printf("Example format: ./15-10 <RD|WR|RDWR> [1-9..]\n"); return 0;
    } else if (3 <= argc) {
        cycle = atoi(argv[2]);
    }
    mkfifo(ffpath, 0600);
    if (!strcmp("RD", argv[1])) {
        fffd = open(ffpath, O_RDONLY | O_NONBLOCK);
    } else if (!strcmp("WR", argv[1])) {
        fffd = open(ffpath, O_WRONLY | O_NONBLOCK);
    } else if (!strcmp("RDWR", argv[1])) {
        fffd = open(ffpath, O_RDWR);
    } else {
        printf("Example format: ./15-10 <RD|WR|RDWR> [1-9..]\n"); return 0;
    }
    printf("open return of fffd: %d\n", fffd);
    while(flag) {
        bzero(line_buf, MAXLINE);
        n = write(fffd, "Parent send", strlen("Parent send"));
        printf("write return: %d\n", n);
        n = read(fffd, line_buf, MAXLINE);
        printf("read return: %d\n", n);
        printf("Parend read: %s\n", line_buf);
        cyc_counter++;
        if (cycle <= cyc_counter) {
            flag = 0;
        }
    }
    return 0;
}
