#include <stdio.h>
#define MAXLINE 1024

int main(int argc, const char *argv[]) {
    char line_buf[MAXLINE];
    FILE *fpin;
    if (2 > argc) {
        printf("You need cmdstring!\n");
        return 0;
    }
    fpin = popen(argv[1], "r");
    while(fgets(line_buf, MAXLINE, fpin)) {
        fputs(line_buf, stderr);
    }
    pclose(fpin);
    return 0;
}
