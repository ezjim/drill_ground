#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>

static volatile sig_atomic_t sigflag;
static sigset_t newmask, oldmask, zeromask;

static void sig_usr(int signo) {
    sigflag = 1;
}

void TELL_WAIT() {
    if (SIG_ERR == signal(SIGUSR1, sig_usr)) {
        printf("Error by signal(SIGUSR1, sig_usr)"); _exit(-1);
    }
    if (SIG_ERR == signal(SIGUSR2, sig_usr)) {
        printf("Error by signal(SIGUSR2, sig_usr)"); _exit(-1);
    }
    sigemptyset(&zeromask);
    sigemptyset(&newmask);
    sigaddset(&newmask, SIGUSR1);
    sigaddset(&newmask, SIGUSR2);
    if (0 > sigprocmask(SIG_BLOCK, &newmask, &oldmask)) {
        printf("SIG_BLOCK error"); _exit(-1);
    }
}

void TELL_PARENT(pid_t pid) {
    kill( pid, SIGUSR1);
}

void WAIT_PARENT() {
    while (0 == sigflag) {
        sigsuspend(&zeromask);
    }
    sigflag = 0;
    if (0 > sigprocmask(SIG_SETMASK, &oldmask, NULL)) {
        printf("SIG_SETMASK error"); _exit(-1);
    }
}

void TELL_CHILD(pid_t pid) {
    kill( pid, SIGUSR2);
}

void WAIT_CHILD() {
    while (0 == sigflag) {
        sigsuspend(&zeromask);
    }
    sigflag = 0;
    if (0 > sigprocmask(SIG_SETMASK, &oldmask, NULL)) {
        printf("SIG_SETMASK error"); _exit(-1);
    }
}

int main(int argc, const char *argv[]) {
    pid_t pid;
    int fd, counter = 0, caps = 10;
    char path[BUFSIZ], flag = 'p';
    if (2 <= argc) {
        caps = atoi(argv[1]);
    }
    sprintf( path, "./pid_%d.counter", getpid());
    fd = open( path, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
    lseek(fd, 0, SEEK_SET);
    write( fd, &counter, sizeof(counter));
    TELL_WAIT();
    if (0 > (pid = fork())) {
        printf("Error by fork!\n");
        return -1;
    } else if (0 == pid) {
        if ('c' == flag) {
            sigflag = 1;
        }
        while (caps > counter) {
            WAIT_PARENT();
            lseek(fd, 0, SEEK_SET);
            read( fd, &counter, sizeof(counter));
            printf("%c[7;33mChild  pid %d current counter: %d%c[0m\n", 27, getpid(), counter, 27);
            counter++;
            lseek(fd, 0, SEEK_SET);
            write( fd, &counter, sizeof(counter));
            TELL_PARENT(getppid());
        }
    } else {
        if ('p' == flag) {
            sigflag = 1;
        }
        while (caps > counter) {
            WAIT_CHILD();
            lseek(fd, 0, SEEK_SET);
            read( fd, &counter, sizeof(counter));
            printf("%c[7;32mParent pid %d current counter: %d%c[0m\n", 27, getpid(), counter, 27);
            counter++;
            lseek(fd, 0, SEEK_SET);
            write( fd, &counter, sizeof(counter));
            TELL_CHILD(pid);
        }
    }
    return 0;
}
