#include <stdio.h>
#include <unistd.h>         // For STDIN_FILENO;
#include <termios.h>
#include <errno.h>
static struct termios save_termios;
static int ttysavefd = -1;
static enum { RESET, RAW, CBREAK } ttystate = RESET;

int tty_raw(int fd) {
    int err;
    struct termios buf;
    if (RESET != ttystate) {
        errno = EINVAL;
        return -1;
    }
    if (0 > tcgetattr(fd, &buf)) {
        return -2;
    }
    save_termios = buf;
    buf.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
    buf.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
    buf.c_cflag &= ~(CSIZE | PARENB);
    buf.c_cflag |= CS8;
    buf.c_oflag &= ~(OPOST);
    buf.c_cc[VMIN] = 1;
    buf.c_cc[VTIME] = 0;
    if (0 > tcsetattr(fd, TCSAFLUSH, &buf)) {
        return -3;
    }
    if (0 > tcgetattr(fd, &buf)) {
        err = errno;
        tcsetattr(fd, TCSAFLUSH, &save_termios);
        errno = err;
        return -4;
    }
    if ((buf.c_lflag & (ECHO | ICANON | IEXTEN | ISIG)) ||
            (buf.c_iflag & (BRKINT | ICRNL | INPCK | ISTRIP | IXON)) || 
            (buf.c_cflag & (CSIZE | PARENB | CS8)) != CS8 ||
            (buf.c_oflag & OPOST) || buf.c_cc[VMIN] != 1 ||
            buf.c_cc[VTIME] != 0) {
        tcsetattr(fd, TCSAFLUSH, &save_termios);
        errno = EINVAL;
        return -5;
    }
    ttystate = RAW;
    ttysavefd = fd;
    return 0;
}

int main(int argc, const char *argv[]) {
    printf("return %d\n", tty_raw(STDIN_FILENO));
    return 0;
}
