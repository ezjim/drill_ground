#include <stdio.h>
#include <netdb.h>          // For struct sockaddr_in...;
#include <errno.h>          // For errno;
#include <string.h>         // For strerror;
#include <signal.h>         // For signal;
#include <sys/wait.h>       // For wait;
#include <sys/socket.h>

#define BUFLEN 128
#define QLEN 10

void sig_wait_fun(int signo) {
    while(-1 != wait(NULL)) {};
}

void server(int sockfd) {
    int clfd;
    FILE *fp;
    char buf[BUFLEN];
    pid_t pid;
    for(;;) {
        clfd = accept(sockfd, NULL, NULL);
        // 所有新来连接让子进程去处理具体逻辑；
        if (0 == (pid = fork())) {
            if (0 > clfd) {
                fprintf(stderr, "ruptimed: accept error: %s", strerror(errno));
                _Exit(1);
            }
            if (NULL == (fp = popen("/usr/bin/uptime", "r"))) {
                sprintf(buf, "error: %s\n", strerror(errno));
                send(clfd, buf, strlen(buf), 0);
            } else {
                while(NULL != fgets(buf, BUFLEN, fp)) {
                    send(clfd, buf, strlen(buf), 0);
                }
                pclose(fp);
            }
            // shutdown(clfd, SHUT_RDWR);
            close(clfd);
            _Exit(0);
        }
        // 或在此启用该close，或启用子进程中的shutdown；
        close(clfd);
    }
}

int main(int argc, const char *argv[]) {
    struct sockaddr_in serv_addr;
    int sockfd, err, n, on = 1, LISTEN_PORT = 9999;
    char *LISTEN_ADDR = "0.0.0.0";
    // 避免子进程变僵尸；
    signal(SIGCHLD, sig_wait_fun);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(inet_addr(LISTEN_ADDR));
    serv_addr.sin_port = htons(LISTEN_PORT);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (-1 == sockfd) {
        fprintf(stderr, "error by socket: %s\n", strerror(errno)); _Exit(-1);
    }
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));
    if (0 > bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr))) {
        fprintf(stderr, "error by bind: %s\n", strerror(errno)); _Exit(-1);
    }
    if (0 > listen(sockfd, QLEN)) {
        fprintf(stderr, "error by listen: %s\n", strerror(errno)); _Exit(-1);
    }
    server(sockfd);
    return 0;
}
