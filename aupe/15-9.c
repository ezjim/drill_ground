#include <stdio.h>

#define MAXLINE 1024

int main(int argc, const char *argv[]) {
    char *rst_c;
    char line_buf[MAXLINE];
    FILE *fpin;
    printf("pid: %d\n", getpid());
    fpin = popen(argv[1], "r");
    while((rst_c = fgets(line_buf, MAXLINE, fpin))) {
        printf("fgets returned %s", rst_c);
        fputs(line_buf, stdout);
    }
    printf("fgets returned %s\n", rst_c);
    pclose(fpin);
    return 0;
}
