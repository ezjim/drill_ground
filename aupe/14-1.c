#include <stdio.h>
#include <fcntl.h>

int fd;
char buf[BUFSIZ];

int lock_reg( int fd, int cmd, int type, off_t offset, int whence, off_t len) {
    struct flock lock;
    lock.l_type = type;         /* F_RDLCK, F_WRLCK, F_UNLCK */
    lock.l_start = offset;      /* byte offset, relative to l_whence */
    lock.l_whence = whence;     /* SEEK_SET, SEEK_CUR, SEEK_END */
    lock.l_len = len;           /* #byte (0 means to EOF) */
    return (fcntl( fd, cmd, &lock));
}

#define read_lock( fd, offset, whence, len) \
            lock_reg( (fd), F_SETLK, F_RDLCK, (offset), (whence), (len))
#define readw_lock( fd, offset, whence, len) \
            lock_reg( (fd), F_SETLKW, F_RDLCK, (offset), (whence), (len))
#define write_lock( fd, offset, whence, len) \
            lock_reg( (fd), F_SETLK, F_WRLCK, (offset), (whence), (len))
#define writew_lock( fd, offset, whence, len) \
            lock_reg( (fd), F_SETLKW, F_WRLCK, (offset), (whence), (len))
#define un_lock( fd, offset, whence, len) \
            lock_reg( (fd), F_SETLK, F_UNLCK, (offset), (whence), (len))

void reading_lock() {
    while(1) {
        read_lock( fd, 0, SEEK_SET, 1);
        lseek( fd, 0, SEEK_SET);
        read( fd, buf, 1);
        printf("pid: %d %s\n", getpid(), buf);
        sleep(2);
        un_lock( fd, 0, SEEK_SET, 1);
    }
}

int main(int argc, const char *argv[]) {
    pid_t pid;
    char path[] = "./tmplock";
    fd = open( path, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
    unlink( path);
    write( fd, "a", 1);
    if (0 < (pid = fork())) {
        if (0 < (pid = fork())) {
            sleep(1);       // 保证先被读；
            writew_lock( fd, 0, SEEK_SET, 1);
            lseek( fd, 0, SEEK_SET);
            write( fd, "b", 1);
            un_lock( fd, 0, SEEK_SET, 1);
        } else {
            // 两个reading_lock相差1秒交叉执行，最终饿死writew_lock；
            sleep(1);
            reading_lock();
        }
    } else {
        reading_lock();
    }
    return 0;
}
