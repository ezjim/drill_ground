#include <stdio.h>
#include <unistd.h>
#include <limits.h>

int main(int argc, const char *argv[]) {
    printf("Process can open %ld fd!\n", sysconf(_SC_OPEN_MAX));
    return 0;
}
