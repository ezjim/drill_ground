#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <sys/resource.h>
#define BUFSIZE 10

static void sig_xfsz() {
    fprintf(stderr, "captured the signal SIGXFSZ\n");
}

void * signal_intr(int signo, void *func) {
    struct sigaction act, oact;
    act.sa_handler = func;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
#ifdef SA_INTERRUPT
    act.sa_flags |= SA_INTERRUPT;
    fprintf(stderr, "SA_INTERRUPT defined\n");
#endif
    if (0 > sigaction(signo, &act, &oact)) {
        return(SIG_ERR);
    }
    return(oact.sa_handler);
}

int main(int argc, const char *argv[]) {
    int rn, wn;
    char buf[BUFSIZE];
    struct rlimit rl;

    if (SIG_ERR == signal_intr(SIGXFSZ, sig_xfsz)) {
        fprintf(stderr, "Error by signal(SIGXFSZ, sig_usr)"); _exit(-1);
    }
    getrlimit(RLIMIT_FSIZE, &rl);
    fprintf(stderr, "rlimit_cur: %10ld\n", rl.rlim_cur);
    fprintf(stderr, "rlimit_max: %10ld\n", rl.rlim_max);
    rl.rlim_cur = BUFSIZE + 5;
    setrlimit(RLIMIT_FSIZE, &rl);
    getrlimit(RLIMIT_FSIZE, &rl);
    fprintf(stderr, "rlimit_cur: %10ld\n", rl.rlim_cur);
    fprintf(stderr, "rlimit_max: %10ld\n", rl.rlim_max);
    while (0 < (rn = read(STDIN_FILENO, buf, BUFSIZE))) {
        wn = write(STDOUT_FILENO, buf, rn);
        if (rn != wn) {
            fprintf(stderr, "write error\n"); // return (wn);
        }
    }
    if (0 > rn) {
        fprintf(stderr, "read error\n"); _exit(rn);
    }
    return 0;
}
