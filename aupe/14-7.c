#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

int lock_reg( int fd, int cmd, int type, off_t offset, int whence, off_t len) {
    struct flock lock;
    lock.l_type = type;         /* F_RDLCK, F_WRLCK, F_UNLCK */
    lock.l_start = offset;      /* byte offset, relative to l_whence */
    lock.l_whence = whence;     /* SEEK_SET, SEEK_CUR, SEEK_END */
    lock.l_len = len;           /* #byte (0 means to EOF) */
    return (fcntl( fd, cmd, &lock));
}

#define read_lock( fd, offset, whence, len) \
            lock_reg( (fd), F_SETLK, F_RDLCK, (offset), (whence), (len))
#define readw_lock( fd, offset, whence, len) \
            lock_reg( (fd), F_SETLKW, F_RDLCK, (offset), (whence), (len))
#define write_lock( fd, offset, whence, len) \
            lock_reg( (fd), F_SETLK, F_WRLCK, (offset), (whence), (len))
#define writew_lock( fd, offset, whence, len) \
            lock_reg( (fd), F_SETLKW, F_WRLCK, (offset), (whence), (len))
#define un_lock( fd, offset, whence, len) \
            lock_reg( (fd), F_SETLK, F_UNLCK, (offset), (whence), (len))

void print_fdflag(int fd) {
    int val;
    if (0 > (val = fcntl( fd, F_GETFL, 0))) {
        printf("fcntl error for fd %d\n", fd); exit(-1);
    }
    switch (val & O_ACCMODE) {
        case O_RDONLY:
            printf("read only");
            break;
        case O_WRONLY:
            printf("write only");
            break;
        case O_RDWR:
            printf("read write");
            break;
        default:
            printf("unknown access mode");
    }
    if (val & O_APPEND) {
        printf(", append");
    }
    if (val & O_NONBLOCK) {
        printf(", nonblocking");
    }
    if (val & O_SYNC) {
        printf(", synchronous writes");
    }
    printf("\n");
}

int main(int argc, const char *argv[]) {
    pid_t pid;
    int fd, caps = 10, counter = 0;
    char path[BUFSIZ] = "./tmpfile", flag = 'p';
    if (2 <= argc) {
        caps = atoi(argv[1]);
    }
    fd = open( path, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
    unlink( path);
    lseek(fd, 0, SEEK_SET);
    write( fd, &flag, sizeof(flag));
    if (0 > (pid = fork())) {
        printf("Error by fork!\n");
        return -1;
    } else if (0 == pid) {
        while (caps > counter) {
//          下面while代码块即类 WAIT_PARENT();
            while( 'c' != flag) {
                read_lock( fd, 0, SEEK_SET, 1);
                lseek(fd, 0, SEEK_SET);
                // if (-1 == pread( fd, &flag, sizeof(flag), 0)) {
                if (-1 == read( fd, &flag, sizeof(flag))) {
                    printf("Error by child read!\n");
                }
                un_lock( fd, 0, SEEK_SET, 1);
                // 增宽另一个进程writew_lock获得锁的窗口；
                usleep(1);
            }
//          下面从writew_lock至un_lock代码块即类 TELL_PARENT();
            writew_lock( fd, 0, SEEK_SET, 1);
            printf("%c[7;33mChild  pid %d current flag: %c%c[0m\n", 27, getpid(), flag, 27);
            flag = 'p';
            counter++;
            lseek(fd, 0, SEEK_SET);
            // if (-1 == pwrite( fd, &flag, sizeof(flag), 0)) {
            if (-1 == write( fd, &flag, sizeof(flag))) {
                printf("Error by child write!\n");
            }
            un_lock( fd, 0, SEEK_SET, 1);
        }
    } else {
        while (caps > counter) {
//          下面while代码块即类  WAIT_CHILD();
            while( 'p' != flag) {
                read_lock( fd, 0, SEEK_SET, 1);
                lseek(fd, 0, SEEK_SET);
                // if (-1 == pread( fd, &flag, sizeof(flag), 0)) {
                if (-1 == read( fd, &flag, sizeof(flag))) {
                    printf("Error by parent read!\n");
                }
                un_lock( fd, 0, SEEK_SET, 1);
                // 增宽另一个进程writew_lock获得锁的窗口；
                usleep(1);
            }
//          下面从writew_lock至un_lock代码块即类 TELL_CHILD();
            writew_lock( fd, 0, SEEK_SET, 1);
            printf("%c[7;32mParent pid %d current flag: %c%c[0m\n", 27, getpid(), flag, 27);
            flag = 'c';
            counter++;
            lseek(fd, 0, SEEK_SET);
            // if (-1 == pwrite( fd, &flag, sizeof(flag), 0)) {
            if (-1 == write( fd, &flag, sizeof(flag))) {
                printf("Error by parent write!\n");
            }
            un_lock( fd, 0, SEEK_SET, 1);
        }
    }
    return 0;
}
