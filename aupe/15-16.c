#include <stdio.h>
#include <sys/sem.h>    // For semget...;
#include <sys/shm.h>    // For shmget...;
#include <string.h>     // For strerror;
#include <errno.h>

#define SIZE sizeof(long)

union semun {
    int              val;    /* Value for SETVAL */
    struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
    unsigned short  *array;  /* Array for GETALL, SETALL */
    struct seminfo  *__buf;  /* Buffer for IPC_INFO
                                (Linux-specific) */
};

static int update(long *ptr) {
    return((*ptr)++);
}

int main(int argc, const char *argv[]) {
    int shm_id, sem_id, i, counter, nloops = 10;
    char flag = 'p';
    pid_t pid;
    union semun seun;
    struct sembuf sebf[1];
    void *shm_area;
    if (2 <= argc) {
        nloops = atoi(argv[1]);
    }
    sebf[0].sem_num = 0;
    sebf[0].sem_flg = SEM_UNDO;
    sem_id = semget(IPC_PRIVATE, 1, IPC_CREAT);
    if ('p' == flag) {
        seun.val = 0;   // 父进程先执行；
    } else {
        seun.val = 2;   // 子进程先执行；
    }
    // 给第一个信号量赋初值0，序数从0开始；
    semctl(sem_id, 0, SETVAL, seun);
    printf("the semid_ds[0].semval is %d\n", semctl(sem_id, 0, GETVAL));
    shm_id = shmget(IPC_PRIVATE, SIZE, IPC_CREAT);
    if (0 > (pid = fork())) {
        fprintf(stderr, "fork error"); _Exit(-1);
    } else if (0 < pid) {
        shm_area = shmat(shm_id, 0, 0);
        for (i=0; i<nloops; i+=2) {
            // SEM_UNDO仅执行一次，不然每次都会给 信号量调整值中累加；
            if (2==i) {
                sebf[0].sem_flg = 0;
            }
            // 注释中包含@符的，表示可与其上语句替换，
            // 实现资源量分配、限制作用；
            sebf[0].sem_op = 0;
            // @ sebf[0].sem_op = -1;
            semop(sem_id, sebf, 1);
            if (i != (counter = update((long *)shm_area))) {
                fprintf(stderr, "%s\n", strerror(errno));
                fprintf(stderr, "parent: expected %d, got %d", i, counter); _Exit(-1);
            }
            printf("%c[7;32mparent current | i:%d | update() return: %d | share value: "\
                    "%2ld|%c[0m\n", 27, i, counter, *(long *)shm_area, 27);
            sebf[0].sem_op = 2;
            // @ sebf[0].sem_op = 1;
            semop(sem_id, sebf, 1);
        }
        shmdt(shm_area);
    } else {
        shm_area = shmat(shm_id, 0, 0);
        for (i=1; i<nloops+1; i+=2) {
            // SEM_UNDO仅执行一次，不然每次都会给 信号量调整值中累加；
            if (3==i) {
                sebf[0].sem_flg = 0;
            }
            sebf[0].sem_op = -1;
            semop(sem_id, sebf, 1);
            if (i != (counter = update((long *)shm_area))) {
                fprintf(stderr, "%s\n", strerror(errno));
                fprintf(stderr, "child: expected %d, got %d", i, counter); _Exit(-1);
            }
            printf("%c[7;33mchild  current | i:%d | update() return: %d | share value: "\
                    "%2ld|%c[0m\n", 27, i, counter, *(long *)shm_area, 27);
            sebf[0].sem_op = -1;
            // @ sebf[0].sem_op = 1;
            semop(sem_id, sebf, 1);
        }
        shmdt(shm_area);
    }
    waitpid(pid, NULL, 0);
    return 0;
}
