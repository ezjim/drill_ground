#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
int main(int argc, const char *argv[]) {
    pid_t pid, pgid;
    pgid = getpid();
    int32_t c_p;
    if (2 > argc) {
        printf(" Need more argv!\n");
        printf(" Usage: ./multi-process 10\n");
        return argc;
    }
    for ( c_p=0; c_p<atoi(argv[1]); c_p++) {
        if (0 > (pid = fork())) {
            printf( "error by fork!\n");
        } else if (0 == pid) {
            printf( "Child %d created!\n", c_p);
        }
    }
    sleep(100);
    return 0;
}
