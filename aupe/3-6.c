#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

void get_current_position(int fd) {
    off_t currpos;
    currpos = lseek(fd, 0, SEEK_CUR);
    printf("Current position is: %ld\n", currpos);
}

int main(int argc, const char *argv[])
{
    int fd1;
    char buffer[BUFSIZ];
    fd1 = open("./out.log", O_RDWR | O_APPEND | O_CREAT);
    get_current_position(fd1);
    write(fd1, "|XXX", sizeof("|XXX"));
    get_current_position(fd1);
    lseek(fd1, 15, SEEK_SET);
    get_current_position(fd1);
    read(fd1, buffer, 10);
    printf("Characters, read: \"%s\"\n", buffer);
    write(fd1, "|YYY", sizeof("|YYY"));
    get_current_position(fd1);
    return 0;
}
