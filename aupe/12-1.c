#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

pthread_mutex_t lock1 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t lock2 = PTHREAD_MUTEX_INITIALIZER;

void prepare(void) {
    printf("preparing locks...\n");
    pthread_mutex_lock(&lock1);
    pthread_mutex_lock(&lock2);
}

void parent(void) {
    printf("parent unlocking locks...\n");
    pthread_mutex_unlock(&lock1);
    pthread_mutex_unlock(&lock2);
}

void child(void) {
    printf("child unlocking locks...\n");
    pthread_mutex_unlock(&lock1);
    pthread_mutex_unlock(&lock2);
}

void * thr_fn(void *arg) {
    printf("thread started...\n");
    pause();
    return(0);
}

int main(int argc, const char *argv[]) {
    int err;
    pid_t pid;
    pthread_t tid;
    setvbuf( stdout, NULL, _IONBF, 0);
    if (0 != (err = pthread_atfork(prepare, parent, child))) {
        printf("can't install fork handlers\n"); _exit(-1);
    }
    if (0 != (err = pthread_create(&tid, NULL, thr_fn, 0))) {
        printf("can't create thread\n"); _exit(-1);
    }
    usleep(1);
    printf("parent about to fork...\n");
    if (0 > (pid = fork())) {
        printf("fork failed\n"); _exit(1);
    } else if (0 == pid) {
        printf("child returned from fork\n");
    } else {
        printf("parent returned from fork\n");
    }
    return 0;
}
