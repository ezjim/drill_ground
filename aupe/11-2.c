#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

// job对象的内存被释放时，rj_lock不能存在于对象内；
static pthread_rwlock_t rj_lock;
struct job {
    struct job *j_next;
    struct job *j_prev;
    int j_id;
};

struct queue {
    struct job *q_head;
    struct job *q_tail;
    pthread_rwlock_t q_lock;
};

// 传递给线程的参数结构体；
struct thr_args {
    int margin;
};

static struct queue q_job;
// 给每个线程实例化一个参数结构体；
struct thr_args thr_argv_01;
struct thr_args thr_argv_02;

int queue_init(struct queue *qp) {
    int err;
    qp->q_head = NULL;
    qp->q_tail = NULL;
    err = pthread_rwlock_init(&qp->q_lock, NULL);
    if (0 != err) {
        return err;
    }
    return 0;
}

void job_insert(struct queue *qp, struct job *jp) {
    pthread_rwlock_wrlock(&qp->q_lock);
    jp->j_next = qp->q_head;
    jp->j_prev = NULL;
    if (NULL != qp->q_head) {
        qp->q_head->j_prev = jp;
    } else {
        qp->q_tail = jp;
    }
    qp->q_head = jp;
    pthread_rwlock_unlock(&qp->q_lock);
}

void job_append(struct queue *qp, struct job *jp) {
    pthread_rwlock_wrlock(&qp->q_lock);
    jp->j_next = NULL;
    jp->j_prev = qp->q_tail;
    if (NULL != qp->q_tail) {
        qp->q_tail->j_next = jp;
    } else {
        qp->q_head = jp;
    }
    qp->q_tail = jp;
    pthread_rwlock_unlock(&qp->q_lock);
}

void job_remove(struct queue *qp, struct job *jp) {
    pthread_rwlock_wrlock(&qp->q_lock);
    if (jp == qp->q_head) {
        qp->q_head = jp->j_next;
        if (qp->q_tail == jp) {
            qp->q_tail = NULL;
        }
    } else if (jp == qp->q_tail) {
        qp->q_tail = jp->j_prev;
        if (qp->q_head == jp) {
            qp->q_head = NULL;
        }
    } else {
        jp->j_prev->j_next = jp->j_next;
        jp->j_next->j_prev = jp->j_prev;
    }
    // 释放job对象时，需加锁，避免另一线程更新它时出问题；
    pthread_rwlock_wrlock(&rj_lock);
    free(jp);
    pthread_rwlock_unlock(&rj_lock);
    pthread_rwlock_unlock(&qp->q_lock);
}

struct job * job_find(struct queue *qp, pthread_t id) {
    struct job *jp;
    if (0 != pthread_rwlock_rdlock(&qp->q_lock)) {
        return NULL;
    }
    for (jp = qp->q_head; jp != NULL; jp = jp->j_next) {
        if (pthread_equal(jp->j_id, id)) {
            break;
        }
    }
    pthread_rwlock_unlock(&qp->q_lock);
    return jp;
}

void update_job_id(struct job *jp, int j_id) {
    // 加锁目的和删除它像对应；
    pthread_rwlock_wrlock(&rj_lock);
    jp->j_id = j_id;
    pthread_rwlock_unlock(&rj_lock);
}

void * thr_worker(void *arg) {
    int i;
    struct job *jp, *next_jp;
    jp = q_job.q_head;
    while (NULL != jp) {
        // 读job对象时，避免被删除；
        pthread_rwlock_rdlock(&q_job.q_lock);
        if (0 == ((struct thr_args *) arg)->margin && jp->j_id > (q_job.q_tail->j_id / 2)) {
            if (0 == (jp->j_id % 2)) {
                update_job_id( jp, jp->j_id + 1);
            }
            if (0 == (jp->j_id % 5)) {
                printf("thread ID is %ld remove jp : %d\n", pthread_self(), jp->j_id);
                // jp对象将被删除，这里迭代它的下一个对象给它；
                next_jp = jp->j_next;
                pthread_rwlock_unlock(&q_job.q_lock);
                job_remove( &q_job, jp);
                pthread_rwlock_rdlock(&q_job.q_lock);
                jp = next_jp;
            }
        } else {
            usleep(1);
        }
        if ((((struct thr_args *) arg)->margin) == (jp->j_id % 2)) {
            if (0 == (jp->j_id % 2)) {
                printf("%c[7;32mthread ID is %ld : %c[0m", 27, pthread_self(), 27);
            } else {
                printf("%c[7;33mthread ID is %ld : %c[0m", 27, pthread_self(), 27);
            }
            printf("job id: %d\n", jp->j_id);
        }
        jp = jp->j_next;
        pthread_rwlock_unlock(&q_job.q_lock);
        usleep(1);
    }
    pthread_exit((void *) 0);
}

int main(int argc, const char *argv[]) {
    int i, err;
    pthread_t tid1, tid2;
    queue_init( &q_job);

    for (i=1; i<100; i++) {
        struct job *jb = malloc((unsigned int) sizeof(struct job));
        jb->j_id = i;
        job_append( &q_job, jb);
    }

    thr_argv_01.margin = 0;
    thr_argv_02.margin = 1;
    pthread_create( &tid1, NULL, thr_worker, &thr_argv_01);
    pthread_create( &tid2, NULL, thr_worker, &thr_argv_02);
    // 等待线程结束再退出进程；
    pthread_join( tid1, NULL);
    pthread_join( tid2, NULL);

    return 0;
}
