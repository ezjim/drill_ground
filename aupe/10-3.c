#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <setjmp.h>

static jmp_buf env_alrm;
static void sig_alrm(int signo) {
    longjmp(env_alrm, 1);
}

unsigned int sleep2(unsigned int nsecs) {
    if (signal(SIGALRM, sig_alrm) == SIG_ERR) {
        return (nsecs);
    }
    // 申明跳入点；
    // longjmp携带参数1过来时，判断将为false，跳过里面逻辑；
    if (setjmp(env_alrm) == 0) {
        alarm(nsecs);
        // 看alarm和用户输入的SIGINT哪个先打断pause()，
        // 过早打断的话，alarm(0)返回剩余秒数，不然，返回0；
        pause();                
    }
    return (alarm(0));
}

static void sig_int(int signo) {
    int i,j;
    volatile int k;
    printf("\nsig_int starting\n");
    for (i=0; i<300000; i++) {
        for (j=0; j<4000; j++) {
            k += i*j;
        }
    }
    printf("sig_int finished\n");
}

int main(int argc, const char *argv[]) {
    unsigned int unslept;
    if (signal(SIGINT, sig_int) == SIG_ERR) {
        printf("signal(SIGINT) error!\n");
    }
    unslept = sleep2(5);
    printf("sleep2 returned: %u\n", unslept);
    return 0;
}
