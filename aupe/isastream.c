#include <stdio.h>
#include <string.h>
#include <stropts.h>
#include <errno.h>
#include <fcntl.h>

int main(int argc, const char *argv[]) {
    int i, fd, rst;
    for (i=1; i<argc; i++) {
        printf("%s ", argv[i]);
        if (0 > (fd = open( argv[i], O_RDONLY))) {
            printf("error by open\n");
            continue;
        }
        rst = isastream( fd);
        switch (rst) {
            case 1:
                  printf("is a stream\n");
                  break;
            case 0:
                 printf("is not\n");
                 break;
            case -1:
                 printf("%s\n", strerror( errno));
                 break;
            default:
                 printf("error by rst\n");
                 break;
        }
    }
    return 0;
}
