#include <stdio.h>
#include <pthread.h>

pthread_t tid;
void * pth_fn(void *arg) {
    tid = pthread_self();
    printf("Thread      pid: %u thread id: %lu %016lx\n", getpid(), tid, tid);
    return ((void *) 3);
}

int main(int argc, const char *argv[]) {
    tid = pthread_self();
    int tret;
    printf("Main thread pid: %u thread id: %lu %#016lx\n", getpid(), tid, tid);
    pthread_create(&tid, NULL, pth_fn, NULL);
    sleep(1);
    pthread_join( tid, (void *) &tret);
    printf("Exit code: %d\n", tret);
    return 0;
}
