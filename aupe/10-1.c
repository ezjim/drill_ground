#include <stdio.h>
#include <sys/types.h>
#include <signal.h>

static void sig_user(int signo) {
    switch (signo) {
        case SIGUSR1: {
            printf("Capture the SIGUSR1\n");
        }
        break;
        case SIGUSR2: {
            printf("Capture the SIGUSR2\n");
        }
        break;
        default:
            printf("Does not recognize signo: %d\n", signo);
    }
}

int main(int argc, const char *argv[]) {
    signal( SIGUSR1, sig_user);
    signal( SIGUSR2, sig_user);
    pause();
    return 0;
}
