#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <sys/resource.h>
#define BFSZ 1024 * 1024 * 1200

char tmp_buf[BFSZ];
char io_buf[BFSZ / 3];
static int n, fn;
static void sig_alarm(int signo) {
    printf("captured the signal SIGALRM at %d\n", time(NULL));
//    _exit(-1);
}

int main(int argc, const char *argv[]) {
    FILE *fp = NULL;
    char last_str[] = "File End!";
    struct sigaction act;
    act.sa_handler = sig_alarm;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    act.sa_flags |= SA_INTERRUPT;
    sigaction(SIGALRM, &act, NULL);
    fp = fopen("/dev/urandom", "r");
//     fp = fopen("./tmp2.file", "r");
    printf("Begin read\n");
    n = fread(&tmp_buf, sizeof(char), BFSZ, fp);
    memcpy( &tmp_buf[BFSZ - sizeof(last_str)], last_str, sizeof(last_str));
    printf("Finished, Read %d bytes\n", n);
    fp = fopen("./tmp.file", "w+");
    setvbuf(fp, io_buf, _IOFBF, BFSZ / 3);
    alarm(1);
    printf("Begin write, set alarm at %d\n", time(NULL));
    n = fwrite(&tmp_buf, sizeof(char), BFSZ, fp);
//    n = write(fp->_fileno, &tmp_buf, BFSZ);
    printf("Finnished, write %d bytes\n", n);
    return 0;
}
