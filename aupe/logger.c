#include <stdio.h>
#include <syslog.h>
#include <string.h>
#include <stdarg.h>

// below in syslog.h
// #define LOG_EMERG   0   /* system is unusable */
// #define LOG_ALERT   1   /* action must be taken immediately */
// #define LOG_CRIT    2   /* critical conditions */
// #define LOG_ERR     3   /* error conditions */
// #define LOG_WARNING 4   /* warning conditions */
// #define LOG_NOTICE  5   /* normal but significant condition */
// #define LOG_INFO    6   /* informational */
// #define LOG_DEBUG   7   /* debug-level messages */

// #define LOG_LEVEL LOG_INFO
#define LOG_LEVEL LOG_DEBUG

static char LOG_PATH[] = "/var/log/my-server.log";
static char log_buf[BUFSIZ];
static FILE *LOG_FP;

int logger_init() {
    LOG_FP = fopen(LOG_PATH, "a+");
    setvbuf(LOG_FP, NULL, _IOLBF, BUFSIZ);
    return 0;
}

void logger( int level, char *fmt, ...) {
    if (level <= LOG_LEVEL) {
        va_list argptr;
        va_start(argptr, fmt);
        // vprintf与printf类似，不同的是，它用一个参数取代了变长参数表；
        vprintf( fmt, argptr);
        // 每次重读可变参数，需重置其头；
        va_start(argptr, fmt);
        vsprintf( log_buf, fmt, argptr);
        fwrite( log_buf, sizeof(char), strlen(log_buf), LOG_FP);
        va_end(argptr);
    }
}

int main(int argc, const char *argv[])
{
    logger_init();
    logger( LOG_INFO, "loginfo pid %d argc %d %s\n", 9, 3, "RRRRRR");
    logger( LOG_DEBUG, "logdebug pid %d argc %d\n", getpid(), argc);
    return 0;
}
